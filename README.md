# Soal 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:

## 1a
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13

## 1b
Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

## 1c
Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

## 1d
Setiap data yang terencode akan masuk dalam file “Wibu.log” 

## 1e
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

## Penyelesaian
Untuk mengerjakan soal ini, pertama membuat fungsi untuk menencode teks menjadi rot13
dan atbash cipher. Kodenya adalah sebagai berikut:
```
char rot13_encode_char(char src)
{
    if ((src >= 'A' && src <= 'Z') || (src >= 'a' && src <= 'z'))
    {
        if (src > 109 || (src > 77 && src < 91))
        {
            // Characters that wrap around to the start of the alphabet
            src -= 13;
        }
        else
        {
            // Characters that can be safely incremented
            src += 13;
        }
    }
    
    return src;
}
```
```
char base64_encode_char(char* src)
{
    if (!src)
    {
        return 1;
    }

    BIO *bio, *base64;
    FILE *fptr;
    char *result;
    int encodeSize = 4 * ceil((double)strlen(src) / 3);

    result = (char *)malloc((encodeSize + 1) * sizeof(char));
    fptr = fmemopen(result, encodeSize + 1, "w");
    base64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(fptr, BIO_NOCLOSE);
    bio = BIO_push(base64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); // Ignore newlines - write everything in one line

    BIO_write(bio, src, strlen(src));

    BIO_flush(bio);
    BIO_free_all(bio);
    fclose(fptr);
    return result[0];
}
```
Untuk bagian rot13, karakter akan ditambah secara 13 seperti di dalam ASCII. Jika dalam
karakter itu kemungkinan wrap around maka akan dikurang secara 13. Untuk base64 akan
menggunakan base64 encode yang tersedia dalam library openssl. Pertama dengan membuka BIO pointers dan membukanya kedalam character yang diinginkan. Lalu akan dibaca
dengan fungsi BIO_write. Resultnya setelah itu akan di return dalam bentuk char.

Setelah itu, untuk proses log akan menggunakan function logWrite yang akan menulis ke dalam wibu.txt
sesuai mode antara decode atau enkripsi. Kode yang dipakai adalah sebagai berikut:
```
void logWrite(char *oldFileName, char *newFileName, unsigned int mode)
{
    FILE *fptr = fopen(dirPath, "a");
    if(fptr == NULL)
    {
        perror("Error opening log file");
        return;
    }

    if(mode == DECODE)
    {
        fprintf(fptr, "RENAME terdecode %s -> %s\n", oldFileName, newFileName);
    }
    else if(mode == ENCRPYT)
    {
        fprintf(fptr, "RENAME terenkripsi %s -> %s\n", oldFileName, newFileName);
    }
    
    fclose(fptr);
    return;
}
```
Untuk FUSE nya akan menggunakan .getattr, .readdir, dan .read. Kodenya adalah sebagai berikut:
```
static int anyaGetAttr(const char *path, struct stat *st)
{
    printf("Debug:: .getattr called\n");
    char newPath[1024];
    char *renamePtr;

    if((renamePtr = strstr(path, encryptKeyword)) != NULL)
    {
        int lengthOfKeyword = strlen(encryptKeyword);
        renamePtr += lengthOfKeyword;
        char *cutRenamePtr = strstr(renamePtr, "/");

        if(cutRenamePtr != NULL)
        {
            cutRenamePtr++;
            encode(cutRenamePtr);
        }
    }

    if(strcmp(path, "/") == 0)
    {
        strcpy(newPath, dirPath);
    }
    else
    {
        sprintf(newPath, "%s%s", dirPath, path);
    }

    int result = lstat(newPath, st);
    if(result == -1)
    {
        perror("Error in lstat\n");
        return -errno;
    }

    return 0;
}
```
```
static int anyaReaddir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    printf("Debug:: .readdir called\n");
    int result = 0;

    char newPath[1024];
    char *renamePtr;

    if((renamePtr = strstr(path, encryptKeyword)) != NULL)
    {
        int lengthOfKeyword = strlen(encryptKeyword);
        renamePtr += lengthOfKeyword;
        char *cutRenamePtr = strstr(renamePtr, "/");

        if(cutRenamePtr != NULL)
        {
            cutRenamePtr++;
            encode(cutRenamePtr);
        }
    }

    (void) offset;
    (void) fi;
    char *tempStr = (char*) malloc(sizeof(char) * 1024);
    
    // Open dirent
    DIR *dp;
    struct dirent *de;
    dp = opendir(path); // Buat nanti pathnya diganti ke path nama wibu
    if(dp == NULL)
    {
        perror("Error opening directory");
        return -errno;
    }

    while((de = readdir(dp)) != NULL)
    {
        // Create stat struct
        struct stat st;
        memset(&st, 0, sizeof(stat));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(strcmp(de->d_name, ".") || strcmp(de->d_name, ".."))
        {
            result = (filler(buf, de->d_name, &st, 0));
        }
        else
        {
            strcpy(tempStr, de->d_name);
            encode(tempStr);
            result = (filler(buf, tempStr, &st, 0));
            strcpy(tempStr,"");
        }
    }

    free(tempStr);
    return 0;
}
```
```
static int anyaRead(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    
    printf("Debug:: .read called\n");
    int result = 0;

    char newPath[1024];
    char *renamePtr;

    if((renamePtr = strstr(path, encryptKeyword)) != NULL)
    {
        int lengthOfKeyword = strlen(encryptKeyword);
        renamePtr += lengthOfKeyword;
        char *cutRenamePtr = strstr(renamePtr, "/");

        if(cutRenamePtr != NULL)
        {
            cutRenamePtr++;
            encode(cutRenamePtr);
        }
    }

    int fileDescriptor;
    (void)fi;

    fileDescriptor = open(path, O_RDONLY);

    if (fileDescriptor == -1)
    {
        return -errno;
    }

    result = pread(fileDescriptor, buf, size, offset);

    if (result == -1)
    {
        return -errno;
    }

    close(fileDescriptor);
    return result;
}
```

# Soal 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut :

## 2a
Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

## Penyelesaian 
Untuk mengklasifikasikan apakah suatu direktori akan diencode atau didecode, kami membuat fungsi isAnimeku yang mengembalikan nilai true atau false. Ketika isAnimeku bernilai true, yaitu ketika terdapat substring "IAN_", maka direktori path yang dimasukkan akan diencode, sebaliknya ketika bernilai false maka akan di-decode.
Fungsi isIAN:
```
bool isIAN(const char *path) 
{
    for(int i=0;i<strlen(path)-4+1;i++)
        if(path[i] == 'I' && path[i+1] == 'A' && path[i+2] == 'N' && path[i+3] == '_') return 1;
    return 0;
}
```
Fungsi Encode dan Decode  Vigenere Cipher dengan key “INNUGANTENG” (case sensitive)
```
char vigenereCipherEncode(char c, int i)
{
	int addition = vigenereKey[i % vigenereKeyLength] - 'A';
	if (c >= 'a' && c <= 'z')
	{
		return (c - 'a' + addition) % 26 + 'a';
	}
	else if (c >= 'A' && c <= 'Z')
	{
		return (c - 'A' + addition) % 26 + 'A';
	}
	return c;
}

char vigenereCipherDecode(char c, int i)
{
	int addition = vigenereKey[i % vigenereKeyLength] - 'A';
	if (c >= 'a' && c <= 'z')
	{
		return (c - 'a' + 26 - addition) % 26 + 'a';
	}
	else if (c >= 'A' && c <= 'Z')
	{
		return (c - 'A' + 26 - addition) % 26 + 'A';
	}
	return c;
}

```
# Soal 3
Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan :

## 3a
Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial

## 3b
Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.

## 3c
Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.

## 3d
Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).

## 3e
Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.

## Penyelesaian
Untuk menyelesaikan soal ini, dibuat sebuah function untuk mengencode dan mendecode biner, kodenya adalah
sebagai berikut:
```
void convertBinerToDecimal(char decimal[], char binary[])
{
	unsigned long long int decimalValue = 0;
	int length = strlen(binary);
	for (int i = 0; i < length; i++)
	{
		decimalValue <<= 1;
		decimalValue += binary[i] - '0';
	}
	sprintf(decimal, "%d", decimalValue);
}
```
```
void convertDecimalToBinary(char binary[], char decimal[])
{
	unsigned long long int decimalValue = 0;
	int length = strlen(decimal);
	for (int i = 0; i < length; i++)
	{
		decimalValue *= 10;
		decimalValue += decimal[i] - '0';
	}
	
	length = 0;
	for (length = 0; decimalValue > 0; length++)
	{
		binary[length] = decimalValue % 2 + '0';
		decimalValue >>= 1;
	}
	
	char temp;
	for (int i = 0; i < length/2; i++)
	{
		temp = binary[i];
		binary[i] = binary[length - 1 - i];
		binary[length - 1 - i] = temp;
	}
	
	binary[length] = '\0';
}
```

Kendala:
- FUSE yang dibuat masih belum bisa berfungsi dengan baik. Setelah perjalanan harus me-restart sistem agar sistem tersebut dapat diakses lagi.
- FUSE yang dibuat dapat mengcrash sistem secara permanen, salah satu instalasi linux harus di reinstall karena penjalanan program ini.