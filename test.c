#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <math.h>

char *base64_encode(const char *src)
{ //BeNeRiN
    BIO *bio, *base64;
    FILE* fptr;
    char *result;
    int encodeSize = 4 * ceil((double) strlen(src)/ 3);

    result =(char *) malloc((encodeSize + 1) * sizeof(char));
    fptr = fmemopen(result, encodeSize + 1, "w");
    base64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(fptr, BIO_NOCLOSE);
    bio = BIO_push(base64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Ignore newlines - write everything in one line

    BIO_write(bio, src, strlen(src));
    
    BIO_flush(bio);
    BIO_free_all(bio);
    fclose(fptr);
    return result;
}
char *rot13_encode(const char *src)
{
    if(src == NULL){
      return NULL;
    }
  
    char* result = malloc(strlen(src));
    
    if(result != NULL){
      strcpy(result, src);
      char* current_char = result;
      
      while(*current_char != '\0'){
        //Only increment alphabet characters
        if((*current_char >= 97 && *current_char <= 122) || (*current_char >= 65 && *current_char <= 90)){
          if(*current_char > 109 || (*current_char > 77 && *current_char < 91)){
            //Characters that wrap around to the start of the alphabet
            *current_char -= 13;
          }else{
            //Characters that can be safely incremented
            *current_char += 13;
          }
        }
        current_char++;
      }
    }
    return result;
}
int main(int argc, char const *argv[])
{
    const char *test = "Hello World!";
    char *rot13res = rot13_encode(test);
    printf("This is the result of rot13 %s\n",rot13res);

    char *base64res = base64_encode(test);
    printf("This is the result of base64 %s\n",base64res);

    char *usrname = getenv("USER");
    printf("This is usrname: %s\n",usrname);
    return 0;
}
