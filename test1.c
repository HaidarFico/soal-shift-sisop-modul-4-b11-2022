#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>

char *vigenere_encode(const char *src)
{
    char key[] = "INNUGANTENG";
    int msgLen = strlen(src), keyLen = strlen(key), i, j;

    char msg[msgLen];
    while (src[i]){
        msg[i] = toupper(src[i]);
        i++;
    }
    char newKey[msgLen], encryptedMsg[msgLen];

    //generating new key
    for(i = 0, j = 0; i < msgLen; ++i, ++j){
        if(j == keyLen)
            j = 0;
        if (isalpha(src[i])){
            newKey[i] = key[j];
        }
        else{
            newKey[i] = src[i];
            j--;
        }
    }
    newKey[i] = '\0';

    //encryption
    for(i = 0; i < msgLen; ++i){
        if (isalpha(src[i])){
        encryptedMsg[i] = ((msg[i] + newKey[i]) % 26) + 'A';
        }
        else{
        encryptedMsg[i] = msg[i];
        }
        
        if (islower(src[i])){
            encryptedMsg[i] += 32;
        }
    } 
    encryptedMsg[i] = '\0';

    // printf("Encrypted Message: %s\n", encryptedMsg);
    char *result = malloc(msgLen);
    result = encryptedMsg;
    
    return result;
}

int main(){
    char src[] = "idontwannabeyouanymore.txt";
    printf("%s", vigenere_encode(src));
    return 0;
}