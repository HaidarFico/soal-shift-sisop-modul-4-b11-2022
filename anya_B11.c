#define FUSE_USE_VERSION 28
#include <math.h>
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <stdbool.h>
#include <ctype.h>
#define DECODE 0
#define ENCRPYT 1
// Link with -lssl, -lcrypto, and -lm
char dirPath[1024];
static char encryptKeyword[] = "Animeku_";
static char *vigenereKey = "INNUGANTENG";
static int vigenereKeyLength = 11;

char vigenereCipherEncode(char c, int i)
{
	int addition = vigenereKey[i % vigenereKeyLength] - 'A';
	if (c >= 'a' && c <= 'z')
	{
		return (c - 'a' + addition) % 26 + 'a';
	}
	else if (c >= 'A' && c <= 'Z')
	{
		return (c - 'A' + addition) % 26 + 'A';
	}
	return c;
}

char vigenereCipherDecode(char c, int i)
{
	int addition = vigenereKey[i % vigenereKeyLength] - 'A';
	if (c >= 'a' && c <= 'z')
	{
		return (c - 'a' + 26 - addition) % 26 + 'a';
	}
	else if (c >= 'A' && c <= 'Z')
	{
		return (c - 'A' + 26 - addition) % 26 + 'A';
	}
	return c;
}

void convertBinerToDecimal(char decimal[], char binary[])
{
	unsigned long long int decimalValue = 0;
	int length = strlen(binary);
	for (int i = 0; i < length; i++)
	{
		decimalValue <<= 1;
		decimalValue += binary[i] - '0';
	}
	sprintf(decimal, "%d", decimalValue);
}

void convertDecimalToBinary(char binary[], char decimal[])
{
	unsigned long long int decimalValue = 0;
	int length = strlen(decimal);
	for (int i = 0; i < length; i++)
	{
		decimalValue *= 10;
		decimalValue += decimal[i] - '0';
	}
	
	length = 0;
	for (length = 0; decimalValue > 0; length++)
	{
		binary[length] = decimalValue % 2 + '0';
		decimalValue >>= 1;
	}
	
	char temp;
	for (int i = 0; i < length/2; i++)
	{
		temp = binary[i];
		binary[i] = binary[length - 1 - i];
		binary[length - 1 - i] = temp;
	}
	
	binary[length] = '\0';
}


char *base64_encode(const char *src)
{
    if (src == NULL)
    {
        return NULL;
    }

    BIO *bio, *base64;
    FILE *fptr;
    char *result;
    int encodeSize = 4 * ceil((double)strlen(src) / 3);

    result = (char *)malloc((encodeSize + 1) * sizeof(char));
    fptr = fmemopen(result, encodeSize + 1, "w");
    base64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(fptr, BIO_NOCLOSE);
    bio = BIO_push(base64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); // Ignore newlines - write everything in one line

    BIO_write(bio, src, strlen(src));

    BIO_flush(bio);
    BIO_free_all(bio);
    fclose(fptr);
    return result;
}

char *rot13_encode(const char *src)
{
    if (!src)
    {
        return NULL;
    }
    size_t lengthOfString = strlen(src);

    char *result = malloc(lengthOfString);

    strcpy(result, src);
    char *currChar = result;

    while (*currChar != '\0')
    {
        if ((*currChar >= 'A' && *currChar <= 'Z') || (*currChar >= 'a' && *currChar <= 'z'))
        {
            if (*currChar > 109 || (*currChar > 77 && *currChar < 91))
            {
                // Characters that wrap around to the start of the alphabet
                *currChar -= 13;
            }
            else
            {
                // Characters that can be safely incremented
                *currChar += 13;
            }
        }
        currChar++;
    }
    
    return result;
}

char rot13_encode_char(char src)
{
    if ((src >= 'A' && src <= 'Z') || (src >= 'a' && src <= 'z'))
    {
        if (src > 109 || (src > 77 && src < 91))
        {
            // Characters that wrap around to the start of the alphabet
            src -= 13;
        }
        else
        {
            // Characters that can be safely incremented
            src += 13;
        }
    }
    
    return src;
}

char base64_encode_char(char* src)
{
    if (!src)
    {
        return 1;
    }

    BIO *bio, *base64;
    FILE *fptr;
    char *result;
    int encodeSize = 4 * ceil((double)strlen(src) / 3);

    result = (char *)malloc((encodeSize + 1) * sizeof(char));
    fptr = fmemopen(result, encodeSize + 1, "w");
    base64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(fptr, BIO_NOCLOSE);
    bio = BIO_push(base64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); // Ignore newlines - write everything in one line

    BIO_write(bio, src, strlen(src));

    BIO_flush(bio);
    BIO_free_all(bio);
    fclose(fptr);
    return result[0];
}

void encode(char *src)
{
    for(int i = 0; src[i] != '\0'; i++)
    {
        if(islower(src[i]) != 0)
        {
            src[i] = rot13_encode_char(src[i]);
        }
        else if(isupper(src[i]) != 0)
        {
            src[i] = base64_encode_char(src);
        }
    }
}

void logWrite(char *oldFileName, char *newFileName, unsigned int mode)
{
    FILE *fptr = fopen(dirPath, "a");
    if(fptr == NULL)
    {
        perror("Error opening log file");
        return;
    }

    if(mode == DECODE)
    {
        fprintf(fptr, "RENAME terdecode %s -> %s\n", oldFileName, newFileName);
    }
    else if(mode == ENCRPYT)
    {
        fprintf(fptr, "RENAME terenkripsi %s -> %s\n", oldFileName, newFileName);
    }
    
    fclose(fptr);
    return;
}

void renameFile(char *directoryPath)
{

}

static int anyaGetAttr(const char *path, struct stat *st)
{
    printf("Debug:: .getattr called\n");
    char newPath[1024];
    char *renamePtr;

    if((renamePtr = strstr(path, encryptKeyword)) != NULL)
    {
        int lengthOfKeyword = strlen(encryptKeyword);
        renamePtr += lengthOfKeyword;
        char *cutRenamePtr = strstr(renamePtr, "/");

        if(cutRenamePtr != NULL)
        {
            cutRenamePtr++;
            encode(cutRenamePtr);
        }
    }

    if(strcmp(path, "/") == 0)
    {
        strcpy(newPath, dirPath);
    }
    else
    {
        sprintf(newPath, "%s%s", dirPath, path);
    }

    int result = lstat(newPath, st);
    if(result == -1)
    {
        perror("Error in lstat\n");
        return -errno;
    }

    return 0;
}

static int anyaReaddir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    printf("Debug:: .readdir called\n");
    int result = 0;

    char newPath[1024];
    char *renamePtr;

    if((renamePtr = strstr(path, encryptKeyword)) != NULL)
    {
        int lengthOfKeyword = strlen(encryptKeyword);
        renamePtr += lengthOfKeyword;
        char *cutRenamePtr = strstr(renamePtr, "/");

        if(cutRenamePtr != NULL)
        {
            cutRenamePtr++;
            encode(cutRenamePtr);
        }
    }

    (void) offset;
    (void) fi;
    char *tempStr = (char*) malloc(sizeof(char) * 1024);
    
    // Open dirent
    DIR *dp;
    struct dirent *de;
    dp = opendir(path); // Buat nanti pathnya diganti ke path nama wibu
    if(dp == NULL)
    {
        perror("Error opening directory");
        return -errno;
    }

    while((de = readdir(dp)) != NULL)
    {
        // Create stat struct
        struct stat st;
        memset(&st, 0, sizeof(stat));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(strcmp(de->d_name, ".") || strcmp(de->d_name, ".."))
        {
            result = (filler(buf, de->d_name, &st, 0));
        }
        else
        {
            strcpy(tempStr, de->d_name);
            encode(tempStr);
            result = (filler(buf, tempStr, &st, 0));
            strcpy(tempStr,"");
        }
    }

    free(tempStr);
    return 0;
}

static int anyaRead(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    
    printf("Debug:: .read called\n");
    int result = 0;

    char newPath[1024];
    char *renamePtr;

    if((renamePtr = strstr(path, encryptKeyword)) != NULL)
    {
        int lengthOfKeyword = strlen(encryptKeyword);
        renamePtr += lengthOfKeyword;
        char *cutRenamePtr = strstr(renamePtr, "/");

        if(cutRenamePtr != NULL)
        {
            cutRenamePtr++;
            encode(cutRenamePtr);
        }
    }

    int fileDescriptor;
    (void)fi;

    fileDescriptor = open(path, O_RDONLY);

    if (fileDescriptor == -1)
    {
        return -errno;
    }

    result = pread(fileDescriptor, buf, size, offset);

    if (result == -1)
    {
        return -errno;
    }

    close(fileDescriptor);
    return result;
}

// static int anyaMkdir(const char *path, mode_t mode)
// {
//     char newPath[1024];
//     char *tempPtr;
//     if((tempPtr = strstr(path, encryptKeyword)) != NULL)
//     { 
//         renameFile(tempPtr);
//         // rename sesuai encoding
//     }

//     int result = mkdir(newPath, mode);
//     // Write the logs bro

//     if(result == -1)
//     {
//         return -errno;
//     }

//     return result;
// }

static struct fuse_operations operations = {
    .getattr = anyaGetAttr,
    .readdir = anyaReaddir,
    .read = anyaRead,
    // .mkdir = anyaMkdir
    };

int main(int argc, char *argv[])
{
    char *usrname = getenv("USER");

    sprintf(dirPath, "/home/%s/Documents", usrname);
    umask(0);
    return fuse_main(argc, argv, &operations, NULL);
}
